<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePelangganRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => 'required|max:255|string',
            "address" => 'required|max:255|string',
            "phone" => 'required||max:12|string',
            "email" => 'required|email:dns|unique:pelanggans|max:255|string',
        ];
    }
}
