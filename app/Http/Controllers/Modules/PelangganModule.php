<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Responses\Responses;
use App\Models\Pelanggan;

class PelangganModule
{


    public function getAll()
    {
        return response()->json([
            "message" => "success",
            'statusCode' => 200,
            "data" => Pelanggan::all(),
        ]);
    }

    public function created($request)
    {
        $saveData = [
            "name" => $request['name'],
            "address" => $request['address'],
            "phone" => $request['phone'],
            "email" => $request['email'],
        ];
        Pelanggan::create($saveData);
        return;
    }

    public function findOne($id)
    {
        $checkData = Pelanggan::find($id);
        return $checkData;
    }

    public function update($request, Pelanggan $pelanggan)
    {

        $saveData = [
            "name" => $request['name'],
            "address" => $request['address'],
            "phone" => $request['phone'],
            "email" => $request['email'],
        ];
        $pelanggan->update($saveData);
        return;
    }

    public function delete($id)
    {
        try {
            $getData = Pelanggan::find($id);
            Pelanggan::where('id', $id)->delete();
            return $getData;
        } catch (\Throwable $th) {
            return;
        }
    }
}
