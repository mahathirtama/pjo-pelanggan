<?php

namespace App\Http\Controllers;

use App\Models\Kasir;
use App\Http\Requests\StoreKasirRequest;
use App\Http\Requests\UpdateKasirRequest;
use Illuminate\Support\Facades\DB;

class KasirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            "message" => "success",
            'statusCode' => 200,
            "data" => DB::select("
           select
            pelanggans.name, pelanggans.address, pelanggans.phone, pelanggans.email, 
            products.product_name, products.product_category, products.product_description, products.price,
            qty, total, kasirs.created_at
           from kasirs
           inner join pelanggans on kasirs.id_pelanggan = pelanggans.id
           inner join products on kasirs.id_product = products.id
           "),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreKasirRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreKasirRequest $request)
    {
        try {
            $isValidateData = $request->validate([
                "id_pelanggan" => 'required|numeric',
                "id_product" => 'required|numeric',
                "qty" => 'required|numeric',
            ]);
            $idProduct = $isValidateData['id_product'];
            $priceProduct = DB::select("select price from products where id = '$idProduct'");
            $query  = "insert into kasirs values (id, :id_pelanggan, :id_product, :qty, :total, :created_at, :updated_at)";
            DB::connection('mysql')->insert($query, [
                'id_pelanggan' => $isValidateData['id_pelanggan'],
                'id_product' => $isValidateData['id_pelanggan'],
                'qty' => $isValidateData['qty'],
                'total' => $isValidateData['qty'] * $priceProduct[0]->price,
                'created_at' => now(),
                'updated_at' => now()
            ]);
            return response()->json([
                "message" => "success",
                'statusCode' => 200,
                "data" => $isValidateData,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "message" => $th->getMessage(),
                'statusCode' => 400,
                "data" => null
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kasir  $kasir
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $checkData = DB::select("
          select
            pelanggans.name, pelanggans.address, pelanggans.phone, pelanggans.email, 
            products.product_name, products.product_category, products.product_description, products.price,
            qty, total, kasirs.created_at
           from kasirs
           inner join pelanggans on kasirs.id_pelanggan = pelanggans.id
           inner join products on kasirs.id_product = products.id
           where kasirs.id = '$id'
           ");

        if (!$checkData == []) {
            return response()->json([
                "message" => "success",
                'statusCode' => 200,
                "data" => $checkData
            ]);
        } else {
            return response()->json([
                "message" => 'error data tidak di temukan',
                'statusCode' => 404,
                "data" => null
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kasir  $kasir
     * @return \Illuminate\Http\Response
     */
    public function edit(Kasir $kasir)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateKasirRequest  $request
     * @param  \App\Models\Kasir  $kasir
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateKasirRequest $request, Kasir $kasir)
    {
        try {
            $isValidateData = $request->validate([
                "id_pelanggan" => 'required|numeric',
                "id_product" => 'required|numeric',
                "qty" => 'required|numeric',
                "total" => 'required|numeric',
            ]);
            $kasir->update($isValidateData);
            return response()->json([
                "message" => "success",
                'statusCode' => 200,
                "data" => $isValidateData,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "message" => $th->getMessage(),
                'statusCode' => 400,
                "data" => null
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kasir  $kasir
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $getData = Kasir::find($id);
            Kasir::where('id', $id)->delete();
            return response()->json([
                "message" => "success",
                'statusCode' => 200,
                'data' => $getData
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "message" => $th->getMessage(),
                'statusCode' => 400,
            ]);
        }
    }
}
