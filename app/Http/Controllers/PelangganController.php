<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use App\Http\Requests\StorePelangganRequest;
use App\Http\Requests\UpdatePelangganRequest;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Modules\PelangganModule;
use App\Http\Controllers\Responses\Responses;
use Illuminate\Support\Facades\Auth;

class PelangganController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            "message" => "success",
            'statusCode' => 200,
            "data" => Pelanggan::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePelangganRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePelangganRequest $request)
    {
        $created = new PelangganModule;
        $response = new Responses;
        $validator = Validator::make($request->all(), $request->rules());
        if ($validator->fails()) {
            return $response->getResponse($validator->errors(), null, 400);
        }
        $created->created(request());
        return $response->getResponse("success", $request->all(), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $find = new PelangganModule;
        $response = new Responses;
        $checkData =  $find->findOne($id);

        if (!$checkData == []) {
            return $response->getResponse("success", $checkData, 200);
        } else {
            return $response->getResponse("data todak ditemukan", null, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function edit(Pelanggan $pelanggan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePelangganRequest  $request
     * @param  \App\Models\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePelangganRequest $request, Pelanggan $pelanggan)
    {

        $update = new PelangganModule;
        $response = new Responses;
        $validator = Validator::make($request->all(), $request->rules());
        $update->update(request(), $pelanggan);
        if ($validator->fails()) {
            return $response->getResponse($validator->errors(), null, 400);
        }
        return $response->getResponse("success", $request->all(), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
             $delete = new PelangganModule;
        $response = new Responses;
           $getData =  $delete->delete($id);
            return $response->getResponse("success", $getData,200);
        } catch (\Throwable $th) {
            return $response->getResponse($th->getMessage(), null, 400);
        }
    }
}
