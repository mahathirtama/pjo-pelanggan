<?php
namespace App\Http\Controllers\Responses;
class Responses
{
    public function getResponse($message, $data, $code)
    {
        
        return response()->json([
            "message" => $message,
            'statusCode' => $code,
            "data" => $data
        ]);
    }
}
